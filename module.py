import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import make_scorer, accuracy_score

def percent_plot(data, title):
    '''
    INPUT: data- data of which to graph distribution
            title- graph title
    OUTPUT: Distribution of Data by Percentage Points
    '''
    ax = data.plot(kind='bar')
    plt.title(title)
    ax.yaxis.set_major_formatter(PercentFormatter())
    plt.show()

def rfc_model(randomState, X_train, X_test, y_train, y_test):
    '''
    INPUT:
            randomState - the random state parameter for random forest classifier
            X_train - training set split for the independent variables
            X_test - testing set split for independent variables
            y_train - training set split for the dependent variables
            y_test - testing set split for the dependent variables
    OUTPUT: prints accuracy of the random forest classifier
    '''
    rand_forest = RandomForestClassifier()
    rand_forest.fit(X_train, y_train)
    forest_test_predictions = rand_forest.predict(X_test)
    print(accuracy_score(y_test, forest_test_predictions))
    
    #importance of features rounded to nearest 3 decimals
    print(rand_forest.feature_importances_.round(3))
    

def print_important_features(features, df):
    '''
    INPUT: 
        features- the column number of feature you want to print
        df - the df you want to do this operation on
    OUTPU: prints the name of each feature that you have chosen
    '''
    for col in features:
        print(df.columns[col])