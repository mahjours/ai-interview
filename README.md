# Introduction
Welcome to the technical interview. Thank you for your interest in being part of our team. This will be divided into 2 parts:

1. A recorded video from you talking about your experience
2. A practical test (code challenge)

# About you
We want to know more about you. So we created some questions and we would like you to record yourself answering those questions and send us back the video file. For that, we recommend this website: https://webcamera.io/. With it, you will be able to record yourself and save the video into a file. This will video file should be sent together with your code (described in the sections below). We **will not**, ever, publish or share your video with anyone. This will only be seen by the technical reviewer assigned to your application.

If you *refuse* to record yourself but still want to participate in our process you may only record an audio of you answering the questions below. For audio recording, you may find a lot of options on the web.

### NOTE: So I grew up with developmental stuttering from a very young age, which has persisted throughout adulthood, so I'm not 100% comfortable providing my own voice responses to these questions - I don't want this circumstance to negatively affect your perception of me as a candidate for this role, as I truly love machine learning. Furthermore, with my own voice recording, you'll find that the interview would take at least 40 minutes to complete. As a result, I've manually typed out responses to those questions, as seen in the README file. For good measure, I've leveraged Amazon Polly's S3 option to synthesize an audio-based .mp3 file of my responses (text-to-speech). Please let me know whether either of these alternatives are acceptable or not for this portion of the interview. If they are not, then I'll do my best to provide my own audio recording. Thanks. 

## Questions
The questions we would like you to answer while recording yourself are (please answer in the same order):

    1. What is your experience with machine learning development in production?

With respect to my experience deploying ML models to production, I'm familiar with Amazon Elastic Beanstalk, Google Cloud Engine, PythonAnywhere, and Microsoft Azure - more specifically, I leverage the Flask framework when building side projects for the web and have used all these cloud-based services. I'd say I score about an 8.5 out of 10 in terms of my familiarity with each production environment.


    2. What is your familiarity with Python 3?

So I use Python 3 on a day-to-day basis when building ML models, developing web apps, and experimenting with new frameworks, libraries, and APIs. Python 3 is definitely the version of Python that I use the most and I'd have to say I score about a 9.0 out of 10.


    3. Can you briefly describe your experience with NumPy, Pandas and other scientific Python libraries

Given the degree of computation required in machine learning when building models using different algorithms (SVM, Random Forest, neural networks, etc), NumPy is essential in my day-to-day experience in data science. Obviously libraries like TensorFlow and PyTorch automate gradient descent calculations, performing optimizations, and reducing error loss - but NumPy is still important when performing certain miscellaneous numerical operations. Pandas is also essential for manipulating data during the preprocessing phase (extract, transform, load). I've also used the OpenAI Gym environment several times - primary when doing Deep Reinforcement Learning for research-oriented tasks. I'd also say my average experience with these libraries is about a 9.0 out of 10 across the board.   


    4. Do you have any experience with Scikit Learn or Keras?

Yes, so Scikit Learn is great for importing preloaded datasets and calling helper methods in both preprocessing and classification / regression operations. If neural networks aren't required, for example, then Scikit Learn offers an array of tools for doing machine learning at higher levels of abstraction. I've also used Keras (built on top of TensorFlow) when doing deep learning tasks at a quick and easy pace. All in all, my experience with these libraries ranks a 9.0 out of 10.  


    5. What is your experience with NLP? Do you know libraries such as NLTK and Spacy?

So I've done some NLP work using Recurrent Neural Networks before (introduced by Andrew Ng's online course) - mostly when doing learning exercises and side projects. Though I've heard of NLTK, I've only ever used it a few times and didn't get too involved. And I've never heard of Spacy before. So I guess my experience here averages to about a 8.0 out of 10 (though I'm confident I can pick them up quickly!). 


    6. Can you tell the difference between Precision, Recall and F1 score?

So precision is the ratio of correctly predicted positive observations relative to the total predicted positive observations, recall is the ratio of correctly predicted positive observations to the all observations in actual class, and the F1 score is the weighted average of precision and recall - this score takes both false positives and false negatives into account; it is not as easy to understand as accuracy, but F1 is usually more useful than accuracy, especially if you have an uneven class distribution.


    7. How do you evaluate your classification models?

So the most often method I use is a confusion matrix, which shows the number of correct and incorrect predictions made by the classification model compared to the actual outcomes (target value) in the data - this is illustrated in a tabular format, with the goal of determining the following measurements: Accuracy - the proportion of total number of predictions that were correct, Precision - the proportion of positive cases that were correctly identified, Negative Predictive Value - proportion of negative cases that were correctly identified, Recall - the proportion of actual positive cases which are correctly identified, and Specificity - the proportion of actual negative cases which are correctly identified.


    8. Which classification algorithms do you have most familiarity?

I'm most familiar with the following classification algorithms, as they are the most frequently used ones I've worked with while doing learning exercises, side projects, and online courses: Logistic Regression, Support Vector Machines, Decision Trees, Random Forest, Neural Networks, and K-Nearest Neighbor.


    9. Whats your experience with prediction (regression)?

Regression is useful for predicting outputs that are continuous, meaning that the prediction is represented by a quantity that can be flexibly determined based on the inputs of the model rather than a set of encoded labels. I've had the most experience with Linear Regression, due to its simplicity and versatility in many recurrent situations. And I know that regression trees and support vector regression are also pretty good algorithms to consider if you’re looking for something more sophisticated.   


    10. What are the advantages and disadvantages of neural networks?

So a major advantage of neural networks is that traditional Machine Learning algorithms will certainly reach a level, where more data doesn't improve their performance. Neural Networks, however, are advantageous in that their predictive power greatly improves when you feed them more and more data. Another reason is that much more computational power is available today, enabling us to process more data. Furthermore, breakthroughs in the development of the algorithms make them run much faster than before, making it even easier to feed them big data. I think a major disadvantage of Neural Networks, however, is their “black box” nature, meaning you don’t know how and why your NN came up with a certain output. Another disadvantage is that NNs usually require much more data than traditional Machine Learning algorithms, as in at least thousands if not millions of labeled samples - this can become computationally expensive than traditional algorithms. 


    11. Which clustering algorithms do you have most familiarity?

So I'm most familiar with the following clustering algorithms: K-Means, Gaussian Mixture Models (GMMs), and Hierarchical Clustering (HC). Though in practice, I've used K-Means most frequently in my data science adventures, GMMs two or three times, and Hierarchical Clustering about once or twice in my life.  

    12. Can you describe your workflow when developing a new model with a new dataset?

With respect to my average workflow anytime I build a model with a new dataset, I typically follow these guidelines: define the objective, import the data, explore and clean the data, model an initial baseline, improve on the model, communicate the results, and postulate a reasonable conclusion. The objective phase addresses the problem you're trying to solve, more from a business perspective. Data imports are then made something SQL or the Pandas library (I prefer Pandas!). In the exploration process, I decide on three things: whether this is a supervised or an unsupervised learning problem, whether it's a classification or a regression problem, and whether it's a prediction or an inference problem - I then do data cleaning: I deal with missing values in the data, plot graphs for visualization, and perform the necessary feature engineering. In the model baseline phase, I implement the model of choice (say linear regression), perform data preprocessing, use a train-test-split, and follow up with cross-validation to make initial predictions. After this phase, I try to improve the model (reduce overfitting) by tuning the hyperparameters - adjusting the learning rate, for example. Finally, I produce a data science report either via a tabularized confusion matrix or a brief one-page summary of the results. I then conclude whether and to what extent (if any) that the business objective was in fact achieved.  
    
    13. Do you have experience with Spark or big data tools for machine learning?

Yes, I possess a fair amount of experience with Spark for machine learning - more at an introductory level, however. More specifically, I've implemented general (non-deep) machine learning algorithms in Spark using Scala and Python - and in both cases used DataBricks and Jupyter Notebook environments. I'd like to do more on the big data side of ML, as I now rely on Google Colab for tackling Kaggle Challenges and for developing more serious projects. I'd probably give myself a 8.0 out of 10 ranking here. 

    14. Can you give examples of scenarios where machine learning is not the best option and where do you think we should apply machine learning?

Yes, so let me mention one concrete example: state of the art deep learning algorithms, which realize successful training of really deep Neural Networks, can take several weeks to train completely from scratch, whereas traditional Machine Learning Algorithms take less time to train, ranging from a few minutes to a few hours or days. Though there are some cases where NN’s deal well with little data, most of the time they don’t. In one such case, a simple algorithm like Naive Bayes, which deals much better with little data, would be the appropriate choice than NNs. The fact of the matter is that Neural Networks usually require much more data than traditional Machine Learning algorithms. This isn’t an easy problem to and many Machine Learning problems can be solved well with less data if you use other algorithms.


After recording yourself and downloading the video file, please, watch it and ensure everything is ok. We will declassify you if the video is inaudible or corrupted.

# Code Challenge

## Getting started
Before you start working on the code:

1. Log in into your Bitbucket account or create a new one
2. Fork this repository into a **private repository** (to protect your privacy) [How to fork](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)
3. Clone your forked version of this repository [How to clone](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html)
4. Be sure that you are using Python version >= 3.6

## Challenge
Find more about your challenge in the `challenge.ipynb` file. If Jupyter (IPython) notebooks are not familiar to you, read more at the [Jupyter Notebook docs](https://jupyter-notebook.readthedocs.io/en/stable/).

## Wrapping up.
1. Be sure that your outputs (described in the last section of the `challenge.ipynb` file) are finished.
2. Commit your changes. [How to commit](https://confluence.atlassian.com/get-started-with-bitbucket/push-code-to-bitbucket-861185309.html)
3. Don't forget to add your video file!
4. Push your changes. [How to push](https://confluence.atlassian.com/get-started-with-bitbucket/push-code-to-bitbucket-861185309.html)
5. **If you completed all tasks**: at your repository's page: go to _Settings_, then _User and group access_ and add a new user `marcellus777` with _read_ permission. **Do this step only if you are done**.

**Do not give me access to your repository if you are not done.**

Thank you and good luck!